set datafile separator ','
set logscale xy
set xlabel "chain length"
set ylabel "ops/s"
set grid
set term png
set output "plot.png"
plot "jmh-result.csv" every ::1::13 using 8:($5*$8):($6*$8) title "inline substitution" linetype -1 linecolor 1 linewidth 2 with errorlines, '' every ::14::26 using 8:($5*$8):($6*$8) title "linearization" linetype -1 linecolor 2 with errorlines
