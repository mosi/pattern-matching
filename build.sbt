name := "pattern-matching"

version := "0.1"

scalaVersion := "2.13.5"

libraryDependencies ++= Seq(
  "org.scalatest" %% "scalatest" % "3.2.7" % "test"
)

enablePlugins(JmhPlugin)