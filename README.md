# Nonlinear pattern matching

This is the companion repository for the paper "Nonlinear pattern matching in rule-based modeling languages".
It contains Scala implementations of the algorithms described in the paper as well as code to reproduce the benchmark plot.
To run the code, you need [sbt](https://www.scala-sbt.org/), to generate the plot you need [gnuplot](http://www.gnuplot.info/).

## Algorithms

The three algorithms described in the paper are implemented in the following files:

- `src/main/scala/com/toromtomtom/matching/linear/Matching.scala` contains the linear pattern matching algorithm
- `src/main/scala/com/toromtomtom/matching/nonlinear/Linearization.scala` contains the algorithm for nonlinear pattern matching via linearization
- `src/main/scala/com/toromtomtom/matching/nonlinear/InlineSubstitution.scala` contains the algorithm for nonlinear pattern matching via inline substitution

The directories `src/test/scala/com/toromtomtom/matching/*` contain some simple tests for the algorithms.
The tests can be run with `sbt test`.

## Benchmark

`sbt jmh:run -rf` executes the benchmarks and stores the results in the CSV file `jmh-results.csv` in the project root.
(This will take a while.)
Then, `gnuplot plot.gnuplot` generates the plot as seen in the paper.
