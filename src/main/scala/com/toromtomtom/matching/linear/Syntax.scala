package com.toromtomtom.matching.linear

object Syntax {

  sealed trait AttributePattern
  case class Variable(name: String) extends AttributePattern //with Expression
  case class Constant(value: Int) extends AttributePattern //with Expression

  case class Pattern(name: String, attributes: AttributePattern*)

  def isLinear(patterns: Seq[Pattern]): Boolean = {
    val varsInPatterns = patterns.flatMap(vars)
    varsInPatterns.size == varsInPatterns.toSet.size
  }

  def vars(pattern: Pattern): Seq[Variable] = pattern.attributes.collect {
    case v: Variable => v
  }

}
