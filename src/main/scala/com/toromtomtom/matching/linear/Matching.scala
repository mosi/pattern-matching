package com.toromtomtom.matching.linear

object Matching {

  import Syntax._
  import com.toromtomtom.matching._

  def matchPatterns(patterns: Seq[Pattern], solution: Solution): Seq[Map[Variable, Int]] = patterns match {
    case head +: tail =>
      for {
        headSub <- matchPattern(head, solution)
        tailSubs <- matchPatterns(tail, solution)
      } yield headSub ++ tailSubs
    case _ => Seq(Map.empty)
  }

  def matchPattern(pattern: Pattern, solution: Solution): Seq[Map[Variable, Int]] =
    (for {
      entity <- solution.keys.toSeq
      if entity.name == pattern.name
      if entity.attributes.size == pattern.attributes.size
    } yield {
      pattern.attributes.zip(entity.attributes).foldLeft(Option(Map.empty[Variable, Int])) {
        case (Some(acc), (v: Variable, att)) => Some(acc + (v -> att))
        case (Some(acc), (Constant(cValue), att)) if cValue == att => Some(acc)
        case _ => None
      }
    }).flatten


}
