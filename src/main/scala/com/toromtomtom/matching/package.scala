package com.toromtomtom


package object matching {

  case class Entity(name: String, attributes: Int*)

  class Solution(val entities: Map[Entity, Int]) {
    val keys: Set[Entity] = entities.keySet
  }

  object Solution {
    def apply(entities: (Entity, Int)*): Solution = new Solution(Map(entities: _*))
  }

  def merge[K, V](substitutions: Seq[Map[K, V]]): Map[K, V] =
    substitutions.flatMap(_.toSeq).toMap

}
