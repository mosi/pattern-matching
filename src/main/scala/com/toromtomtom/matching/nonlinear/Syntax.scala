package com.toromtomtom.matching.nonlinear

object Syntax {

  sealed trait Expression
  case class Addition(left: Expression, right: Expression) extends Expression
  case class Variable(name: String) extends Expression
  case class Constant(value: Int) extends Expression

  case class Pattern(name: String, attributes: Expression*)

  def isMatchable(patterns: Seq[Pattern]): Boolean = {
    val directVars = patterns.flatMap(_.attributes).collect {
      case v: Variable => v
    }
    val allVars = vars(patterns)
    allVars.forall(directVars.contains)
  }

  def vars(expression: Expression): Set[Variable] = expression match {
    case Addition(left, right) => vars(left) ++ vars(right)
    case v: Variable => Set(v)
    case c: Constant => Set.empty
  }

  def vars(patterns: Seq[Pattern]): Set[Variable] = patterns.flatMap(_.attributes).map(vars).fold(Set.empty)(_ ++ _)

  def eval(expression: Expression, substitution: Map[Variable, Int] = Map.empty): Option[Int] = expression match {
    case Addition(left, right) => for {
      l <- eval(left, substitution)
      r <- eval(right, substitution)
    } yield l + r
    case v: Variable => substitution.get(v)
    case Constant(value) => Some(value)
  }

}
