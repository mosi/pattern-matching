package com.toromtomtom.matching.nonlinear

import com.toromtomtom.matching.Solution
import com.toromtomtom.matching.linear.Syntax.AttributePattern
import com.toromtomtom.matching.nonlinear.Syntax._
import com.toromtomtom.matching.linear.{Syntax => Linear}

import java.util.UUID
import scala.annotation.tailrec

object Linearization {

  def toLinear(patterns: Seq[Pattern]): (Seq[Linear.Pattern], Map[Linear.Variable, Expression]) = {
    @tailrec
    def go(input: Seq[Pattern], output: Seq[Linear.Pattern], freshVars: Map[Linear.Variable, Expression]): (Seq[Linear.Pattern], Map[Linear.Variable, Expression]) = {
      input match {
        case Pattern(name, attributes@_*) +: tail =>
          val (transformedAttributes, freshestVars) =
            attributes.foldLeft((Seq.empty[AttributePattern], freshVars)) {
              case ((atts, freshestVars), Constant(value)) => (atts :+ Linear.Constant(value), freshestVars)
              case ((atts, freshestVars), v@Variable(name)) =>
                val freshVar =
                  if (freshestVars.contains(Linear.Variable(name))) {
                    // variable occurred before, create new synonym
                    Linear.Variable(UUID.randomUUID().toString)
                  } else {
                    // first occurrence of this variable
                    Linear.Variable(name)
                  }
                (atts :+ freshVar, freshestVars + (freshVar -> v))
              case ((atts, freshestVars), a: Addition) =>
                val value = eval(a)
                if (value.isDefined) (atts :+ Linear.Constant(value.get), freshestVars)
                else {
                  val freshVar = Linear.Variable(s"fresh variable for ${a}")
                  (atts :+ freshVar, freshestVars + (freshVar -> a))
                }
            }
          go(tail, output :+ Linear.Pattern(name, transformedAttributes: _*), freshestVars)
        case _ => (output, freshVars)
      }
    }

    go(patterns, Seq.empty, Map.empty)
  }

  def matchPatterns(patterns: Seq[Pattern], solution: Solution): Seq[Map[Variable, Int]] = {
    val originalVars = vars(patterns)
    val (transformed, constraints) = toLinear(patterns)
    val allResults = com.toromtomtom.matching.linear.Matching.matchPatterns(transformed, solution)
    for {
      substitution <- allResults
      translatedSubstitution = substitution.collect {
        case (Linear.Variable(name), v) if originalVars.contains(Variable(name)) => (Variable(name), v)
      }
      if constraints.forall { case (variable, expression) =>
        (for (exVal <- eval(expression, translatedSubstitution)) yield exVal == substitution(variable)).getOrElse(false)
      }
    } yield translatedSubstitution
  }
}
