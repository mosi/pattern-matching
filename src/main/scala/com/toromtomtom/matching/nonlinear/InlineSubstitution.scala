package com.toromtomtom.matching.nonlinear

import com.toromtomtom.matching._
import com.toromtomtom.matching.nonlinear.Syntax._

import scala.annotation.tailrec

object InlineSubstitution {

  def substitute(substitution: Map[Variable, Int], pattern: Pattern): Pattern = {
    val Pattern(name, attributes@_*) = pattern
    val substitutedAttributes = attributes.map { a =>
      eval(a, substitution).fold(a)(Constant)
    }
    Pattern(name, substitutedAttributes: _*)
  }

  def exists(pattern: Pattern, solution: Solution): Boolean = {
    val entity = Entity(pattern.name, pattern.attributes.map {
      case Constant(value) => value
    }: _*)
    solution.keys.contains(entity)
  }

  def matchPatterns(patterns: Seq[Pattern], solution: Solution): Seq[Map[Variable, Int]] = {

    def go(patterns: Seq[Pattern], substitution: Map[Variable, Int]): Seq[Map[Variable, Int]] = patterns match {
      case Pattern(name, attributes@_*) +: tail =>
        val firstDirectVar = attributes.zipWithIndex.collectFirst { case (v: Variable, i) => (v, i) }
        firstDirectVar.fold {
          // no variables left, recurse to next pattern
          for {
            completeSubstitution <- go(tail, substitution)
            completelySubstituted = substitute(completeSubstitution, Pattern(name, attributes: _*))
            if exists(completelySubstituted, solution)
          } yield completeSubstitution
        } {
          case (v, i) => (for {
            // substitute the variable that has the first direct occurrence
            entity <- solution.keys.toSeq
            if entity.name == name && entity.attributes.size == attributes.size
            value = entity.attributes(i)
            substituted = patterns.map(substitute(Map(v -> value), _))
            completeSubstitution <- go(substituted, substitution + (v -> value))
          } yield completeSubstitution)
        }
      case _ => Seq(substitution)
    }

    go(patterns, Map.empty)
  }

}
