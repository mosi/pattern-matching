package com.toromtomtom.matching.benchmarks

import com.toromtomtom.matching._
import com.toromtomtom.matching.nonlinear.{InlineSubstitution, Linearization}
import com.toromtomtom.matching.nonlinear.Syntax._
import org.openjdk.jmh.annotations._

import java.util.concurrent.TimeUnit

/**
 * run with jmh:run
 */
class Benchmarks {

  import Benchmarks._

  @Benchmark
  @BenchmarkMode(Array(Mode.Throughput))
  @Warmup(iterations = 1, time = 2000, timeUnit = TimeUnit.MILLISECONDS)
  @Measurement(iterations = 5, time = 2000, timeUnit = TimeUnit.MILLISECONDS)
  def linearization(chainState: ChainState): Unit = {
    import chainState._
    Linearization.matchPatterns(pattern, chain)
  }

  @Benchmark
  @BenchmarkMode(Array(Mode.Throughput))
  @Warmup(iterations = 1, time = 2000, timeUnit = TimeUnit.MILLISECONDS)
  @Measurement(iterations = 5, time = 2000, timeUnit = TimeUnit.MILLISECONDS)
  def inlineSubstitution(chainState: ChainState): Unit = {
    import chainState._
    InlineSubstitution.matchPatterns(pattern, chain)
  }
}

object Benchmarks {

  @State(Scope.Benchmark)
  class ChainState {
    @Param(Array("1", "2", "4", "8", "16", "32", "64", "128", "256", "512", "1024", "2048", "4096"))
    var chainLength = 1

    val x = Variable("x")
    val pattern = Seq(Pattern("A", x), Pattern("A", Addition(Variable("x"), Constant(1))))
    lazy val chain = {
      val entities = for (x <- 1 to chainLength) yield (Entity("A", x) -> 1)
      Solution(entities: _*)
    }
  }
}
