package com.toromtomtom.matching.nonlinear

import com.toromtomtom.matching._
import com.toromtomtom.matching.nonlinear.Linearization._
import com.toromtomtom.matching.nonlinear.Syntax._
import org.scalatest.funsuite.AnyFunSuite

class LinearizationTest extends AnyFunSuite {

  private val x = Variable("x")
  private val y = Variable("y")
  private val sol = Solution(
    Entity("A", 1) -> 1,
    Entity("A", 2) -> 1,
    Entity("A", 3) -> 1,
    Entity("B", 1, 1) -> 1,
    Entity("B", 2, 1) -> 1,
  )

  test("A pattern should have no matches in an empty solution") {
    val emptySol = Solution()
    val pattern = Seq(Pattern("A"))
    val matches = matchPatterns(pattern, emptySol).toSet
    assertResult(Set.empty)(matches)
  }

  test("A pattern should only match entities with the same species name") {
    val pattern = Seq(Pattern("Z", x))
    val matches = matchPatterns(pattern, sol).toSet
    assertResult(Set.empty)(matches)
  }

  test("A pattern with a constant should only match entities with that constant's value") {
    val pattern = Seq(Pattern("B", Constant(1), x))
    val matches = matchPatterns(pattern, sol).toSet
    val expected = Set(Map(x -> 1))
    assertResult(expected)(matches)
  }

  test("An entity pattern with a variable should have one match per entity in the solution") {
    val pattern = Seq(Pattern("A", x))
    val matches = matchPatterns(pattern, sol).toSet
    val expected = Set(
      Map(x -> 1),
      Map(x -> 2),
      Map(x -> 3)
    )
    assertResult(expected)(matches)
  }

  test("Two entity patterns with a variable each should have one match per pair of entities in the solution") {
    val pattern = Seq(Pattern("A", x), Pattern("A", y))
    val matches = matchPatterns(pattern, sol).toSet
    val expected = Set(
      Map(x -> 1, y -> 1),
      Map(x -> 1, y -> 2),
      Map(x -> 1, y -> 3),
      Map(x -> 2, y -> 1),
      Map(x -> 2, y -> 2),
      Map(x -> 2, y -> 3),
      Map(x -> 3, y -> 1),
      Map(x -> 3, y -> 2),
      Map(x -> 3, y -> 3)
    )
    assertResult(expected)(matches)
  }

  test("A pattern with a variable occurring twice should have the correct matches") {
    val pattern = Seq(Pattern("A", x), Pattern("A", x))
    val matches = matchPatterns(pattern, sol).toSet
    val expected = Set(
      Map(x -> 1),
      Map(x -> 2),
      Map(x -> 3)
    )
    assertResult(expected)(matches)
  }

  test("A pattern with a variable occurring twice in different species should have the correct matches") {
    val pattern = Seq(Pattern("A", x), Pattern("B", x, Constant(1)))
    val matches = matchPatterns(pattern, sol).toSet
    val expected = Set(
      Map(x -> 1),
      Map(x -> 2)
    )
    assertResult(expected)(matches)
  }

  test("A pattern of neighboring entities in a chain should match all neighbors") {
    val pattern = Seq(Pattern("A", x), Pattern("A", Addition(Variable("x"), Constant(1))))
    val chainSol = Solution(
      Entity("A", 1) -> 1,
      Entity("A", 2) -> 1,
      Entity("A", 3) -> 1,
      Entity("A", 4) -> 1
    )
    val matches = matchPatterns(pattern, chainSol).toSet
    val expected = Set(
      Map(x -> 1),
      Map(x -> 2),
      Map(x -> 3)
    )
    assertResult(expected)(matches)
  }
}
